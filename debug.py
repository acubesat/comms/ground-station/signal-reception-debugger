import os
import sys

#Get observation parameters
f_center='437' #Frequency in MHz
bandwidth='1000000' #Bandwidth in Hz
channels='8192' #FFT size
nbins='100' #T_sample = nbins*channels/bandwidth
duration='60' #Duration of the observation in seconds

f_center = str(float(f_center)*10**6)

#Delete pre-existing observation.dat & plot.png files
try:
    os.remove('observation.dat')
    os.remove('plot.png')
except OSError:
    pass

#Execute top_block.py with parameters
sys.argv = ['AcubeSAT_COMMS_debug.py', '--c-freq='+f_center, '--samp-rate='+bandwidth, '--nchan='+channels, '--nbin='+nbins, '--obs-time='+duration]
execfile('AcubeSAT_COMMS_debug.py')

#Execute plotter
sys.argv = ['plot.py', 'freq='+f_center, 'samp_rate='+bandwidth, 'nchan='+channels, 'nbin='+nbins]
execfile('plot.py')
